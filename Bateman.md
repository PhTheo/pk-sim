### Beispiel: Einkompartimentmodell, extravasale Applikation

Arzneiform und Körper werden jeweils als ein Kompartiment modelliert.

          +-----------------+            +--------------+
      D   |                 |    k a     |              |    k_e
	----> | Applikationsort | ---------> |  Organismus  | --------->
          |      (A_A)      |            |     (A_P)    |
          +-----------------+            +--------------+

verwendete Symbole:

|        |                                           |
| ------ | ----------------------------------------- |
| $`D`$    | Wirkstoffdosis                            |
| $`A_A`$  | Wirkstoffkonzentration am Applikationsort |
| $`A_P`$  | Wirkstoffkonzentration im Blutplasma      |
| $`k_a`$  | Resorptionsgeschwindigkeitskonstante      |
| $`k_e`$  | Eliminationsgeschwindigkeitskonstante     |

#### Differentialgleichungen 

```math
\begin{aligned}
  \frac{dA_A}{dt} &=& - k a A_A \\
  \frac{dA_P}{dt} &=&   k a A_A - k_e A_P \\
\end{aligned}
```
