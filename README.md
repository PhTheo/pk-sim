# Einleitung

## Digitalisierung in der Hochschullehre

Durch den Ausbruch der
[Coronavirus-Krankheit-2019](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Steckbrief.html)
und der dadurch erforderlich gewordenen Schutzmaßnahmen kann das
Praktikum „Pharmazeutische Technologie“ im Sommersemester 2020 nicht
wie geplant vollständig im Labor stattfinden.

Auch im Hinblick auf die Offensive des Ministeriums für Kultur und
Wissenschaft zur [Digitalisierung der
Lehre](https://www.mkw.nrw/hochschule-und-forschung/digitalisierung-hochschule-und-wissenschaft)
an den Hochschulen in Nordrhein-Westfalen muss geprüft werden, ob sich
einzelne Versuche auf digitale Lehr- und Lernstrukturen übertragen lassen.

## Pharmakokinetische Simulationen

In der pharmazeutischen Technolgie/Biopharmazie sind
pharmakokinetische Simulationen sehr gut für eine Digitalisierung
geeignet – entweder als Ergänzung zu anschaulichen
Analogrechenmodellen wie
[Wasserintegratoren](https://en.wikipedia.org/wiki/Water_integrator)
oder als eigenständige, reine Computeranwendungen.

## Lehr- und Lernziele

### Bachelorstudiengänge

Die Studierenden verwenden pharmakokinetische
Simulationen in digitalen, pharmakokinetischen Kompartimentmodellen.


Damit untersuchen sie den Einfluss von Dosierung,
Geschwindigkeitskonstanten und *linearen* Übergangskinetiken auf die
Konzentration von Arzneistoffen mit verschiedenen
physikalisch-chemischen Eigenschaften in Körpergeweben.

Damit erfassen sie spielerisch das komplexe Zusammenspiel von
Freisetzung, Absorption, Verteilung, Metabolisierung und Exkretion der
Wirkstoffe und können so den Einfluss der physikalisch-chemischen
Wirkstoffeigenschaften sowie verschiedener Formulierungs- und
Herstellungsvariation im Hinblick auf die **Bioverfügbarkeit** ihres
Produktes analysieren, steuern und bewerten.

### Masterstudiengänge

In Masterstudiengängen programmieren die Studierenden die
entsprechenden Modelle selbst.

Sie untersuchen auch *nicht-lineare* Übergangskinetiken.

Sie erarbeiten konkrete *Dosierungsvorschläge* für Arzneimittel mit
Wirkstoffen geringer therapeutischer Breite.

# Theoretische Grundlagen

## Kompartimentmodelle

*Drug delivery*-Systeme und der menschliche (oder tierische) Körper
werden als ein oder mehrere fiktive Räume modelliert; diese Räume
werden *Kompartimente* genannt. 

Die Modellannahmen für ein Kompartiment sind
1) ein Wirkstoff verteilt sich *unverzüglich* im Kompartiment,
2) ein Wirkstoff verteilt sich *homogen* im Kompartiment,
3) der Transport von Wirkstoffen in das und aus dem Kompartiment
   folgen jeweils einer *einheitlichen Kinetik*.


## Übergänge

Die Übergänge zwischen den Kompartimenten werden durch
Differentialgleichungen beschrieben.  Dabei muss die Massenbilanz
immer aufgehen.


## Lösung der Differentialgleichungssysteme


### Analytische Lösungen

Für Systeme linearer Differentialgleichungen kann man grundsätzlich
analytische Lösungen finden. Außer in den einfachsten Fällen sind
diese aber ziemlich komplex und wenig flexibel, wenn die Modelle
erweitert werden sollen.

### Finite-Differenzen-Methode

Die
[Finite-Differenzen-Methode](https://de.wikipedia.org/wiki/Finite-Differenzen-Methode)
kann von den Studierenden selbst programmiert werden – auf jeden Fall
können sie die Programmierung nachvollziehen.

Die Umsetzung der Differentialgleichungen bei linearen Systemen ist
leicht zu implementieren und flexibel an verschiedene Szenarien anpassbar.


### SciPy

Elaboriertere Verfahren zur Lösung von Differentialgleichungssystemen sind z. B. in der
[SciPy](https://www.scipy.org/)-Bibliothek enthalten. Die Verfahren sind
elaborierter und genauer als die Finite-Differenzen-Methode, stellen
sich allerdings als *Black Box* dar.


# Programmierung

## GeoGebra

[Geogebra](https://www.geogebra.org/) ist eine dynamische
Mathematiksoftware, die speziell für Bildungszwecke konzipiert wurde.

Systeme von Differentialgleichungen können direkt in GeoGebra gelöst
und die Lösungen visualisiert werden.

Beispiel: [Einkompartimentsystem mit extravasaler Applikation](https://www.geogebra.org/classic/rjuzr4gd)

![Bateman-Funktion (GeoGebra)](https://gitlab.com/PhTheo/pk-sim/-/raw/master/GeoGebra/Bateman.svg)


## Python

Für die Lösung der den Modellen zu Grunde liegenden
Differentialgleichungssysteme ist die Programmiersprache
[Python](https://www.python.org/) eine gute Wahl. Python ist
universell und frei verfügbar. Die Sprache ist leicht zu lernen und
die Programme gut zu verstehen. Programmierkenntnisse in Python sind
auch in anderen Lern- (oder Lebens-) bereichen nützlich.

### Jupyter-Notebooks

Steht ein geeigneter Notebook-Server
(z. B. [Jupyter-Hub](https://jupyter.org/hub)) zur Verfügung, können
die Programme zusammen mit der dazugehörigen theoretischen Grundlagen
in [Jupyter-Notebooks](https://jupyter.org/) bereitgestellt
werden. Die Programme können direkt in den Notebooks verändert und
ausgeführt werden. Visualisierungen sind mit der
[`matplotlib`](https://matplotlib.org/)-Bibliothek möglich.

Beispiele für die Programmierung eines [Ein-Kompartiment-Modells mit extravasaler Applikation](http://media.dav-medien.de/sample/9783804725362_p.pdf) ([Schiffter 2015](https://www.deutscher-apotheker-verlag.de/shop/produkt/9783804734760/pharmakokinetik-modelle-und-berechnungen)):

- [https://phtheo.gitlab.io/pk-sim/fin-diff/]
- [https://phtheo.gitlab.io/pk-sim/animations/]

Über [binder](https://mybinder.org/) können
[JupyterLab](https://jupyterlab.readthedocs.io/en/stable/getting_started/overview.html)-Notebooks
ohne einen eigenen Server verwendet werden.


### repl.it

[`repl.it`](https://repl.it/) ist ein Onlineservice, der
Arbeitsumgebungen für verschiedene Programmiersprachen, unter anderem
auch Phython und [R](https://www.r-project.org/) anbietet.

Studierende können dort eigene Programme schreiben und ausführen oder
bereitgestellte Programme verändern und ausprobieren.

Beispiel für die Programmierung eines Ein-Kompartiment-Modells mit extravasaler Applikation:

- [https://repl.it/@PhTheo/Bateman]


### PySimpleGUI

Mit Hilfe der
[PySimpleGUI](https://pysimplegui.readthedocs.io/en/latest/)-Bibliothek
lassen sich auch komplexere und ansprechendere graphische Anwendungen
programmieren. Die Programm laufen auch in `repl.it`.


- Beispiel für die Programmierung eines Ein-Kompartiment-Modells mit extravasaler Applikation: [https://repl.it/@PhTheo/BatemanGUI]
  
  <iframe height="400px" width="100%" src="https://repl.it/@PhTheo/BatemanGUI?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

- Beispiel für die Programmierung eines einfachen [Tank-Modells](https://repl.it/@PhTheo/Tank):
[https://repl.it/@PhTheo/Tank?lite=true]

<iframe height="700px" 
        width="100%" 
		src="https://repl.it/@PhTheo/Tank?lite=true" 
		scrolling="no" 
		frameborder="no" 
		allowtransparency="true" 
		allowfullscreen="true" 
		sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals">
</iframe>
