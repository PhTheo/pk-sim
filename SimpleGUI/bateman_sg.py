import PySimpleGUI as sg
import math

""" Drawing the Bateman function in PySimpleGUI """

MAX_X = 500
MAX_Y =  75

def bateman(t, params):
    D, ka, ke = params
    if ka == ke:
        return 100*ka*t*math.exp(-ka*t)
    else:
        return 100*ka/(ka - ke)*(math.exp(-ke*t) - math.exp(-ka*t))


def draw_axis():
    graph.draw_line((0, 0), (MAX_X, 0))                # axis lines # 
    graph.draw_line((0, 0), (0, MAX_Y))

    for x in range(0, MAX_X+1, 100):
        graph.draw_line((x, 0), (x, 1))                # tick marks
        if x != 0:
            # numeric labels
            graph.draw_text(str(x), (x, -2))

    for y in range(0, MAX_Y+1, 25):
        graph.draw_line((0, y), (10, y))
        if y != 0:
            graph.draw_text(str(y), (-10, y))


# Create the graph that will be put into the window
graph = sg.Graph(canvas_size=(500, 300),
                 graph_bottom_left=(-25, -5),
                 graph_top_right=(MAX_X+20, MAX_Y+5),
                 background_color='white',
                 key='graph')

# Window layout
layout = [[sg.Text('Ein-Kompartiment-Modell', font='COURIER 18')],
          [sg.Text('(extravasale Applikation)', font='COURIER 18')],
          [graph],
          [sg.Text('k_a'),
           sg.Slider((1, 500), default_value=300,
                     orientation='h', enable_events=True, key='-KA-'),
           sg.Text('10000/min') ],
          [sg.Text('k_e'),
           sg.Slider((0, 500), default_value=200,
                     orientation='h', enable_events=True, key='-KE-'),
           sg.Text('10000/min'), sg.Quit() ]
]

def draw_graph(values):
    graph.erase()
    draw_axis()
    prev_x = prev_y = None

    for x in range(0, MAX_X):
        y = bateman(x, (100, values['-KA-']/10000, values['-KE-']/10000))
        if prev_x is not None:
            graph.draw_line((prev_x, prev_y), (x, y), color='red')
        prev_x, prev_y = x, y

window = sg.Window('Bateman-Funktion', layout)
window.Finalize()
draw_graph({'-KA-': 300, '-KE-': 200})

while True:
    event, values = window.read()
    if event in [ None, 'Quit']:
        break

    draw_graph(values)
    
window.close()
