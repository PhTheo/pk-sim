#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" tank model for pharmacokinetik simulations """

import PySimpleGUI as sg
import math

sg.theme('System Default')


def order_0(t, params):
    dy, = params
    y = t*dy
    if y < 100:
        return 100 - y
    else:
        return 0

def order_1(t, params):
    k, = params
    return 100*math.exp(-k*t)

def torricelli(t, params):
    k, = params
    if k*t < 10:
        return (10 - k*t)**2
    else:
        return 0

update_function = {
    "0. Ordnung": order_0,
    "1. Ordnung": order_1,
    "Auslauf": torricelli
}

params = {
    "0. Ordnung": [0.5],
    "1. Ordnung": [0.015],
    "Auslauf": [0.05]
}
    

def draw_tank(fill=100, light_color="#4287f5", dark_color = "#1950a8"):
    canvas = window.Element("graph")
    canvas.erase()
    canvas.DrawOval((0, 115), (100, 125), fill_color="light gray")
    canvas.DrawLine((0, 0), (0, 120))
    canvas.DrawLine((100, 0), (100, 120))
    canvas.DrawArc((0, -5), (100, 5), extent=180, start_angle=180, style='arc')
    
    canvas.DrawOval((2, -3.8), (98, 3.8), fill_color=light_color, line_width=0)
    canvas.DrawRectangle((2, 0), (98, fill), fill_color=light_color, line_width=0)
    canvas.DrawOval((2, fill-3.8), (98, fill+3.8), fill_color=dark_color)

def init():
    global t, running
    running = False
    t = 0
    draw_tank()

def update():
    global t
    if running:
        t += 1
        y = update_function[running](t, params[running])
        draw_tank(y)

layout = [
    [ sg.Text("Pharmakokinetisches Tankmodell", font=("Helvetica", 22), justification='center') ],
    [ sg.Graph(canvas_size=(450, 600),
               graph_bottom_left=(-5, -10),
               graph_top_right=(105, 155),
               key="graph") ],
    [ sg.Button('0. Ordnung'), sg.Button('1. Ordnung'), sg.Button('Auslauf'),
      sg.Button('Reset'), sg.Quit() ]
]

window = sg.Window('Tank', layout)
window.Finalize()

init()

while True:
    event, values = window.read(timeout=50)
    if event in (None, 'Quit'):
        break
    elif event in ['Reset']:
        init()
    elif event in ['0. Ordnung', '1. Ordnung', 'Auslauf']:
        running = event

    update()

window.close()
